# Anfisa_test_icecream
### Как запустить проект:
Клонировать проект с gitHub

```
git clone git@github.com:olegvpc/Anfisa_test_icecream.git
```

Работа с gitHub доступна только по Токен или по ssh ключу (не включено в описание)
#### https://github.com/olegvpc/Anfisa_test_icecream

* Cоздать и активировать виртуальное окружение:

```
python3 -m venv venv
```

```
source venv/bin/activate
```

```
python3 -m pip install --upgrade pip
```

* Установить зависимости из файла requirements.txt:

```
pip install -r requirements.txt
```

* Выполнить миграции:

```
python3 manage.py makemigrations
```
```
python3 manage.py migrate
```

* Запустить проект:

```
python3 manage.py runserver
```